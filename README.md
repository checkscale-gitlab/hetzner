# Disclaimer
These terraform templates allows you to create a cheap VM using Hetzner Cloud Provider

# Create API Token 
- Login to https://console.hetzner.cloud
- Find your project
- Select "ACCESS" -> a tab with "API TOKENS"
- Generate API Token and save it in your password manager
- For terraform you will need to export it to environment vars with
```
export TF_VAR_hcloud_token="***"
```

# Template parameters 
```
resource "hcloud_server" "micro-instance" {
  name        = "bot-node"
  image       = "ubuntu-16.04"
  server_type = "cx11"
  ssh_keys    = ["${hcloud_ssh_key.default.name}"]
  location    = "hel1"
  backups     = false
}
```

***server_type*** - (Required, string) Name of the server type this server should be created with  
***image*** - (Required, string) Name or ID of the image the server is created from  
***location*** - (Optional, string) The location name to create the server in. nbg1, fsn1 or hel1  

All the parameter could be found at hetzner provider page -> [link](https://www.terraform.io/docs/providers/hcloud/r/server.html)